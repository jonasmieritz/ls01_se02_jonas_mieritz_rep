/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie dürfen nicht die Namen der Variablen verändern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Ihr Name >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    int anzahlPlaneten =  8;
    
    // Anzahl der Sterne in unserer Milchstraße
       int anzahlSterne = 100000000;
    
    // Wie viele Einwohner hat Berlin?
       int bewohnerBerlin = 3769000;
    
    // Wie alt bist du?  Wie viele Tage sind das?
      short alterTage = 6529;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
      int gewichtKilogramm =   190000;
    
    // Schreiben Sie auf, wie viele km² das größte Land der Erde hat?
      int flaecheGroessteLand = 17100000;
    
    // Wie groß ist das kleinste Land der Erde?
       float flaecheKleinsteLand = 0.44f;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten: " + 8);
    
    System.out.println("Anzahl der Sterne: " + 100000000);
    
    System.out.println("Einwohner Berlins: " + 3769000);
    
    System.out.println("Alter in Tage: " + 6529);
    
    System.out.println("Schwerstes Tier in KG: " + 190000);
    
    System.out.println("Gr��tes Land der Erde " + 17100000 + ("km�"));
    
    System.out.println("Fl�che des kleinsten Landes " + 0.44 + ("km�"));
    
    
    
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}

