import java.util.Scanner;

class Fahrkartenautomat {
    public static void main(String[] args) {
        Scanner tastatur = new Scanner(System.in);
        double zuZahlenderBetrag = fahrkartenbestellungErfassen();
        double rueckgabebetrag = geldeinwurf(zuZahlenderBetrag);
        fahrscheinausgabe();
        r�ckgeldberechnung(rueckgabebetrag);
    }
        
        public static double fahrkartenbestellungErfassen() {
        	Scanner tastatur = new Scanner(System.in);
        	double ticketPreis;
        	int anzahlTickets;
        	double zuzahlenderBetrag;
        	
        	System.out.print("Ticketpreis (EURO-Cent): ");
            ticketPreis = tastatur.nextDouble();

            System.out.print("Anzahl der Tickets: ");
            anzahlTickets = tastatur.nextInt();

            zuzahlenderBetrag = ticketPreis * anzahlTickets;
            
            return zuzahlenderBetrag;
        }
        
        public static double geldeinwurf(double zuZahlenderBetrag) {
        	Scanner tastatur = new Scanner(System.in);
        	 double eingezahlterGesamtbetrag = 0.0;
             while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
                 System.out.format("Noch zu zahlen: %4.2f �%n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
                 System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
                 double eingeworfeneMuenze = tastatur.nextDouble();
                 eingezahlterGesamtbetrag += eingeworfeneMuenze;
                 
             }
             
        	double rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        	
        	return rueckgabebetrag;
        
        	
        }
        
        
        public static void fahrscheinausgabe(){
        	System.out.println("\nFahrschein wird ausgegeben");
            for (int i = 0; i < 8; i++) {
                System.out.print("=");
                try {
                    Thread.sleep(250);
                } catch (InterruptedException e) {

                    e.printStackTrace();
                }
            }
            System.out.println("\n\n");
            
        }
        
        
        public static void r�ckgeldberechnung(double rueckgabebetrag){
        	
             if (rueckgabebetrag > 0.0) {
                 System.out.format("Der R�ckgabebetrag in H�he von %4.2f � %n", rueckgabebetrag);
                 System.out.println("wird in folgenden M�nzen ausgezahlt:");

                 while (rueckgabebetrag >= 2.0)
                 {
                     System.out.println("2 EURO");
                     rueckgabebetrag -= 2.0;
                 }
                 while (rueckgabebetrag >= 1.0) 
                 {
                     System.out.println("1 EURO");
                     rueckgabebetrag -= 1.0;
                 }
                 while (rueckgabebetrag >= 0.5) 
                 {
                     System.out.println("50 CENT");
                     rueckgabebetrag -= 0.5;
                 }
                 while (rueckgabebetrag >= 0.2) 
                 {
                     System.out.println("20 CENT");
                     rueckgabebetrag -= 0.2;
                 }
                 while (rueckgabebetrag >= 0.1) 
                 {
                     System.out.println("10 CENT");
                     rueckgabebetrag -= 0.1;
                 }
                 while (rueckgabebetrag >= 0.05)
                 {
                     System.out.println("5 CENT");
                     rueckgabebetrag -= 0.05;
                 }
             }

             System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
                     + "Wir w�nschen Ihnen eine gute Fahrt.");
         }
        


        int anzahlTickets;
        double zuZahlenderBetrag;
        double ticketPreis;
        double eingezahlterGesamtbetrag;
        double eingeworfeneMuenze;
        double rueckgabebetrag;
        double eingegebenerBetrag;
        
		}