
public class Aufgabe2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.printf("%s%n","0! =                    =    1");
		System.out.printf("%s%n","1! = 1                  =    1");
		System.out.printf("%s%n","2! = 1*2                =    2");
		System.out.printf("%s%n","2! = 1*2*3              =    6");
		System.out.printf("%s%n","2! = 1*2*3*4            =   24");
		System.out.printf("%s%n","2! = 1*2*2*3*4          =  120");
	}

}
